/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gme205_exercise_4;
import java.util.Scanner;

/**
 *
 * @author Administrator
 */
public class DroneSwitch {
    public static void main(String[] args){
        char direction;
        Scanner input = new Scanner(System.in);
        System.out.println("Use w,a,s,d to control Drone direction.");
        System.out.println("Input: ");
        direction = input.next().charAt(0);
        
        switch(direction){
            case 'w':
                System.out.println("Going forward!");
                break;
            case 'a':
                System.out.println("Going left!");
                break;
            case 'd':
                System.out.println("Going right!");
                break;
            case 's':
                System.out.println("Going backward!");
                break;
            default:
                System.out.println("Invalid input!");
                System.out.println("Self destruct in");
                System.out.println("3");
                System.out.println("2");
                System.out.println("1");
                break;
        }
    }
    
}
