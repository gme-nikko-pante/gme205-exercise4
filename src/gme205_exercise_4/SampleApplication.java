/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gme205_exercise_4;
import java.util.Scanner;
/**
 *
 * @author Administrator
 */
public class SampleApplication {
    public static void main(String[] args){
        int choice;
        Scanner input = new Scanner(System.in);
        System.out.println("Greetings! Display your geodata here.");
        do {
            System.out.println("Input 1 for raster or 2 for vector.");
            choice = input.nextInt();
        } while (choice > 2 || choice < 1);
        if (choice == 1){
            System.out.println("Displaying Raster.");
        } else {
            System.out.println("Displaying Vector.");
        }
        System.out.println("Thank you and come back again!");
    }
    
}
